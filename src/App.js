import logo from './logo.svg';
import './App.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPhone } from '@fortawesome/free-solid-svg-icons';
import { faVk } from '@fortawesome/free-brands-svg-icons';

function App() {
	return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo"/>
				<FontAwesomeIcon icon={faVk} className="icon"/>
				<FontAwesomeIcon icon={faPhone} className="icon"/>
				<p>
					Edit <code>src/App.js</code> and save to reload.
				</p>
				{/*<a*/}
				{/*  className="App-link"*/}
				{/*  href="https://reactjs.org"*/}
				{/*  target="_blank"*/}
				{/*  rel="noopener noreferrer"*/}
				{/*>*/}
				{/*  Learn React*/}
				{/*</a>*/}
			</header>
		</div>
	);
}

export default App;
